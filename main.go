package main

import (
	"bufio"
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"log"
	"net/http"
	"strings"
	"syscall"

	"golang.org/x/term"
)

func main() {
	fmt.Print("Enter Password: ")
	passwd, err := term.ReadPassword(int(syscall.Stdin))
	if err != nil {
		log.Fatal("Cannot read password: ", err)
	}
	fmt.Println()

	sha1sum := sha1.Sum(passwd)

	encodedSha1sum := hex.EncodeToString(sha1sum[:])

	response, err := http.Get("https://api.pwnedpasswords.com/range/" + encodedSha1sum[:5])
	if err != nil {
		log.Fatal("Cannot retrieve password range: ", err)
	}
	defer response.Body.Close()

	s := bufio.NewScanner(response.Body)
	for s.Scan() {
		v := strings.Split(s.Text(), ":")
		if len(v) != 2 {
			continue
		}
		if strings.ToLower(v[0]) == encodedSha1sum[5:] {
			fmt.Println("Password found:", v[1])
			return
		}
	}
	fmt.Println("Password not found")
}
